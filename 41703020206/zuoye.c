#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include "cJSON.h"

int main(void)
{
    FILE* fp;

    //以只写方式打开文件
    //fp = fopen("hello.txt", "w");

    //响应消息的地址
    char* response = NULL;
    //响应消息的长度
    size_t resplen = 0;
    //创建内存文件，当通过文件句柄写入数据时，会自动分配内存
    fp = open_memstream(&response,&resplen);
    if(fp == NULL)//打开文件失败，打印错误信息并退出
    {
        perror("open_memstream() failed");
        return EXIT_FAILURE;
    }
    
     

    CURL* curl = curl_easy_init();
    if (curl == NULL)
    {
        perror("curl_easy_init() failed");
        return EXIT_FAILURE;
    }
    
    curl_easy_setopt(curl,CURLOPT_URL,"http://www.nmc.cn/f/rest/aqi/57245");
    curl_easy_setopt(curl,CURLOPT_WRITEDATA,fp);
    curl_easy_setopt(curl,CURLOPT_VERBOSE,1);

    CURLcode error = curl_easy_perform(curl);
    if (error != CURLE_OK)
    {
        fprintf(stderr,"curl_easy_perform() failed:%s\n",curl_easy_strerror(error));
        curl_easy_cleanup(curl);
        return EXIT_FAILURE;
    }

    curl_easy_cleanup(curl);

    //关闭内存文件
    fclose(fp);

    puts(response);

    //解析jSON字符串
    cJSON* json = cJSON_Parse(response);

    if (json == NULL)
    {
        const char* error_pos = cJSON_GetErrorPtr();
        if(error_pos != NULL)
        {
           fprintf(stderr,"Error before: %s\n", error_pos); 
        }
        return EXIT_FAILURE;
    }

    cJSON* forecasttime = cJSON_GetObjectItemCaseSensitive(json,"forecasttime");

    cJSON* aqi = cJSON_GetObjectItemCaseSensitive(json,"aqi");

    cJSON* aq = cJSON_GetObjectItemCaseSensitive(json,"aq");

    printf("空气质量指数：%d\n", aqi->valueint);

    printf("逆境商数：%d\n", aq->valueint);

    cJSON_free(json);

    free(response);
    
    return EXIT_SUCCESS;
}